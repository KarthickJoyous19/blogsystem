<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        <a href="/blogs">    {{ __('Go Back to Blogs') }} </a> <br>
        <a href="/scheduleblogs">    {{ __('Go Back to Schedule Blogs') }} </a>
        </h2>
    </x-slot>


<html>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<style>

.c
{


  display: block;
  margin-left: auto;
  margin-right: auto;
  padding-top: 30px;
  padding-bottom: 30px;

}




.button {
  background-color: black;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

@if ( $schedules->image == null )
<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"> 
  
  <p>Welcome to the blog of <span class="w3-tag" style="border-radius: 5px;"> {{Auth::user()->name}}</span></p>
</header>



<div class="w3-row">

  <div class="w3-card-4 w3-margin w3-white">
    <div class="w3-container">
      <h3 class="w3-container w3-center w3-padding-32"><b></b></h3>
     
  
                   <h1 style="text-align:center;"><b> {{$schedules->title}}</b></h1>
    <div class="w3-container" class="w3-container w3-center w3-padding-32" style="padding-bottom: 10%;">
      <p class="w3-container w3-center "  style="padding-bottom: 30px;">{{$schedules->body}}</p>

    

                <a class="button" style="border-radius: 5px; margin-left: 42%;" href="{{$schedules->id}}/edit"> Edit </a>
                 <a class="button"  style="border-radius: 5px;" href="{{$schedules->id}}/delete"> Delete </a>
                
  </div>
  </div>
  @endif


 

 
@if ( $schedules->image != null )

<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"> 

  <p>Welcome to the blog of <span class="w3-tag" style="border-radius: 5px;"> {{Auth::user()->name}}</span></p>
</header>



<div class="w3-row">

  <div class="w3-card-4 w3-margin w3-white">
    <div class="w3-container">
      <h3 class="w3-container w3-center w3-padding-32"><b></b></h3>
     
  
          <h1 style="text-align:center;"><b> {{$schedules->title}}</b></h1>


             
    <div class="w3-container" class="w3-container w3-center w3-padding-32" style="padding-bottom: 10%;">
      <p class="w3-container w3-center ">{{$schedules->body}}</p>

      
  

     

                       <img src= "{{ asset('uploads/blog/' . $schedules->image) }}" width="270px" height="270px" alt="image" class="c">


                <a class="button" style="border-radius: 5px; margin-left: 42%;" href="{{$schedules->id}}/edit"> Edit </a>
                 <a class="button"  style="border-radius: 5px;" href="{{$schedules->id}}/delete"> Delete </a>

                 
  </div>

  </div>
  @endif

</div>

</div><br>
</div>


<footer class="w3-container w3-black w3-padding-32 w3-margin-top">
<p style="text-align: left;"> <b >Written on :</b> {{$schedules->created_at}}</p>
<p style="text-align: right; margin-top: -15px;"><b >Last Updated On:</b> {{$schedules->updated_at}}</p>
 <p style="text-align: center; margin-top: -15px;"><b >Scheduled Time</b> {{$schedules->scheduled}}</p>

</footer>

</body>
</html>
</x-app-layout>


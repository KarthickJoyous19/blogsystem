<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create a New Blog') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">


              <style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;

 
}


	input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}

.alert {
  padding: 25px;
  background-color: #f44336;
border-radius: 15px;
  height: 20%;
  width: 35%;
  color: black;
  text-align: center;
}


</style>
</head>
<body>


@if (session('success'))

<div class="alert center">
    <strong>{{session('success')}}</strong>
  </div>

@endif 

<form action="{{ route('createblog') }}" method="POST" enctype="multipart/form-data">

	@csrf

  {{csrf_field()}}
  <div class="container">
    
    <p>Please fill in this form to create an new Blog.</p><br>
    <hr>

    <label for="Title"><b>Blog Title (Maximum length is 200) </b></label>
    <input type="text" name="Title" id="Title" autocomplete="off" required maxlength="200">


    <label for="psw"><b>Description</b></label>

     <input style="height: 200px; text-align: initial; margin-top: 3px;" type="text" name="Description" id="Title" autocomplete="off"required>

   
  <input type="hidden" id="name" name="name" value="{{ $name =Auth::user()->name}}" >
    <input type="hidden" id="email" name="email" value="{{ $email =Auth::user()->email}}" >

    
    
        <input type="file" id="image" name="image"  accept="image/png, image/jpg">



 	<button type="submit" class="registerbtn">Create</button>
  </div>
  

</form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
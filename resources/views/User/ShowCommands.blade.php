<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        <a href="/blogs">    {{ __('Go Back to Blogs') }} </a>
        </h2>
    </x-slot>


<html>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<style>

.c
{


  display: block;
  margin-left: auto;
  margin-right: auto;
  padding-top: 30px;
  padding-bottom: 30px;

}




.button {
  background-color: black;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.align

{

padding-left: 20%;
padding-right: 20%;


}
body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}


.Command
{
  height: 40px;
  width: 60%;
}
.alert {
  padding: 25px;
  background-color: #f44336;
border-radius: 15px;
  height: 20%;
  width: 35%;
  color: black;
  text-align: center;


</style>
<body class="w3-light-grey">

    @if (session('success'))

<div class="alert center">
    <strong>{{session('success')}}</strong>
  </div>

@endif 

@if ( $blogs->image == null )
<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"> 
  
  <p>Welcome to the blog of <span class="w3-tag" style="border-radius: 5px;"> {{$blogs->name}}</span></p>
</header>



<div class="w3-row">

  <div class="w3-card-4 w3-margin w3-white">
    <div class="w3-container">
      <h3 class="w3-container w3-center w3-padding-32"></h3>
     
  
                   <h1 style="text-align:center;"><b> {{$blogs->title}}</b></h1>
    <div class="w3-container" class="w3-container w3-center w3-padding-32" style="padding-bottom: 10%;">
      <p class="w3-container w3-center "  style="padding-bottom: 30px;">{{$blogs->body}}</p>

    

            


  </div>

<div class="align">

<div class="w3-row" style="align-content: center; align-items: center;">

  <div class="w3-card-4 w3-margin w3-white">
    <div class="w3-container">
       <div class="w3-container">

<form action="{{ route('addcommand',$blogs->id ) }}" method="post" enctype="multipart/form-data">

  @csrf

  {{csrf_field()}}


<label>Add a Command :</label>
 <input type="text" class="Command" name="comment">


      <input type="hidden" id="name" name="name" value="{{ $name =Auth::user()->name}}" >
    <input type="hidden" id="email" name="email" value="{{ $email =Auth::user()->email}}" >

        <button type="submit" class="button" style="border-radius: 5px;">Submit</button>


       
 <!-- href="/addcommand/{{$blogs->id}}" -->

  </div>
</form>
 @foreach($comments as $comment)
                  @if($comments->id == $blog->id)
<table>
  <tr>
    <td>Comment :</td>
    <td> {{$comments->body}}</td>
  </tr>

  <tr>
    <td> Commented By :</td>
    <td> {{$comments->name}}</td>
  </tr>
</table>
@endif
@endforeach

</div></div></div>

   @endif

 
@if ( $blogs->image != null )

<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"> 

  <p>Welcome to the blog of <span class="w3-tag" style="border-radius: 5px;"> {{$blogs->email}}</span></p>
</header>



<div class="w3-row">

  <div class="w3-card-4 w3-margin w3-white">
    <div class="w3-container">
      <h3 class="w3-container w3-center w3-padding-32"></h3>
     
  
          <h1 style="text-align:center;"><b> {{$blogs->title}}</b></h1>


             
    <div class="w3-container" class="w3-container w3-center w3-padding-32" style="padding-bottom: 10%;">
      <p class="w3-container w3-center ">{{$blogs->body}}</p>

      
  


     


                       <img src= "{{ asset('uploads/blog/' . $blogs->image) }}" width="270px" height="270px" alt="image" class="c">


               


  </div>



<form action="{{ route('addcommand', $blogs->id ) }}" method="post" enctype="multipart/form-data">

  @csrf

  {{csrf_field()}}


<label>Add a Command :</label>
  <input type="text" class="Command" name="comment">
    
      <input type="hidden" id="name" name="name" value="{{ $name =Auth::user()->name}}" >
    <input type="hidden" id="email" name="email" value="{{ $email =Auth::user()->email}}" >

    <button type="submit" class="button" style="border-radius: 5px;">Submit</button>

<!-- href="/addcommand/{{$blogs->id}}" -->


  </div>
</form>

<table>
  <tr>
    <td>Comment :</td>
    <td> {{$comments->body}}</td>
  </tr>

  <tr>
    <td> Commented By :</td>
    <td> {{$comments->name}}</td>
  </tr>
</table>




</table>

  @endif
 
</div>

</div><br>

</div>


<footer class="w3-container w3-black w3-padding-32 w3-margin-top">
<p style="text-align: left;"> <b >Written on :</b> {{$blogs->created_at}}</p>
<p style="text-align: right; margin-top: -15px;"><b >Last Updated On:</b> {{$blogs->updated_at}}</p></footer>

</body>
</html>
</x-app-layout>


<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">

  
            {{ __('All Users') }}
        </h2>
    </x-slot>

    <html>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<style>

.button {
  background-color: black;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.center {
  margin-left: auto;
  margin-right: auto;
  
}
table {

  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 50%;

}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}


.alert {
  padding: 20px;
  background-color: #ABF9C2;
border-radius: 15px;
  height: 20%;
  width: 20%;
  color: black;
  text-align: center;
}

.css-serial {
  counter-reset: serial-number;  /* Set the serial number counter to 0 */
}

.css-serial td:first-child:before {
  counter-increment: serial-number;  /* Increment the serial number counter */
  content: counter(serial-number);  /* Display the counter */
}



</style>

</head>
<body>

@if (session('success'))

<div class="alert center">
    <strong>{{session('success')}}</strong>
  </div>

@endif


<table class="center css-serial">
  <tr>
    <th> S.No</th>
    <th>User Name</th>
    <th>User Email</th>
    <th> Delete User </th>

  </tr>
  
    @foreach($users as $user)
            
               


                     <tr>  
                      <td>  </td>

                      <td><a href="">   {{$user->name}} </a> </td>
                  <td><a href="">   {{$user->email}} </a></td>

              


                    <td>  <a href="<?php echo route('delete', ['id' => $user->id ])?>" class="button">Delete</a></td>


                  
                   </tr>
            

     
               
                    @endforeach</td>
  
  </tr>
  
</table>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        	
            

                	
 					
                    </table>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
</x-app-layout>
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\AdminController;
use App\Http\Middleware\UserAccess;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => ['auth']], function() 

{ 
    



    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');













Route::get('User/blogs', [BlogController::class, 'index'])->name('blogs');

Route::get('User/blogs/{id}', [BlogController::class, 'show']);

// Route::get('User/blogs/edit', [BlogController::class, 'edit'])->name('edit')->middleware(UserAccess::class);

Route::get('User/blogs/{id}/edit', [BlogController::class, 'edit'])->name('edit')->middleware(UserAccess::class);


Route::get('User/{id}/delete', [BlogController::class, 'delete'])->name('delete');



Route::get('User/create', [BlogController::class, 'create'])->name('create');

Route::post('User/schedule', [ScheduleController::class, 'schedule'])->name('schedule');


Route::get('User/createschedule', [ScheduleController::class, 'createschedule'])->name('createschedule');

Route::get('User/scheduleblogs', [ScheduleController::class, 'index'])->name('scheduleblogs');

Route::get('User/schedules/{id}', [ScheduleController::class, 'show']);

Route::post('User/create', [BlogController::class, 'store'])->name('createblog');

Route::post('User/update/{id}', [BlogController::class, 'update'])->name('update');

Route::get('User/allblogs', [BlogController::class, 'allblogs'])->name('allblogs');




Route::get('User/allblogshow/{id}', [BlogController::class, 'allblogshow']);






Route::post('User/updateblogs/{id}', [ScheduleController::class, 'update'])->name('updateblogs');

Route::get('User/schedules/{id}/edit', [ScheduleController::class, 'edit'])->name('edit');

Route::get('User/schedules/{id}/delete', [ScheduleController::class, 'delete'])->name('delete');




Route::post('User/addcommand/{id}', [CommentController::class, 'addcommand'])->name('addcommand');










//Admin


Route::get('Admin/allusers', [AdminController::class, 'allusers'])->name('allusers');


Route::post('Admin/createadmin', [AdminController::class, 'store'])->name('createadmin');


Route::get('Admin/{id}/delete', [AdminController::class, 'delete'])->name('delete');

Route::get('Admin/{id}/deleteB', [AdminController::class, 'deleteBlog'])->name('deleteBlog');

Route::get('Admin/Adminallblogs', [AdminController::class, 'Adminallblogs'])->name('Adminallblogs');










Route::resource('Blog','BlogController');
Route::resource('Schedule','ScheduleController');


});

require __DIR__.'/auth.php';

// Route::resource('blogs','BlogController');


<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;


use App\Providers\RouteServiceProvider;

use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\Rules;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


use App\Models\Blog;
use App\Models\User;
use App\Models\Comment;
use App\Models\Role;





class AdminController extends Controller
{
  

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


 public function __construct()
    {
        $this->middleware('auth');
    }
public function allusers()
{


        $users = User::all();
      
          $this->authorize('allusers', $users);
        
     return view('Admin/users')->with('users',$users);
   



}

public function createadminview()
{
  $this->authorize('createadminview');
return view('Admin/adminregister');

}


public function store(Request $request)
{

$request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
              'password' => Hash::make($request->password),
        ]);

        // $user->attachRole('2');

         $user->attachRole($request->role_id);
       
     

        event(new Registered($user));

 
        return redirect('Admin/allusers')->with('success','New User Created Successfully');

          

}

        public function delete($id)
    {
        DB::delete('delete from users where id=?',[$id]);
        
        return redirect('/Admin/allusers')->with('success','User Deleted Successfully!');
    }





   public function deleteBlog($id)
    {
        DB::delete('delete from blogs where id=?',[$id]);
        return redirect()->back()->with('success','Blog Deleted Successfully!');
    }


   public function Adminallblogs()
    {
        //

        $blogs = Blog::all();
          $this->authorize('Adminallblogs');
        
     return view('Admin/adminallblogs')->with('blogs',$blogs);
    }




    
}

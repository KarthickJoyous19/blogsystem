<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Blog;
use App\Models\User;
use App\Models\Comment;



class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        //

        $blogs = Blog::all();

  

        
     return view('User/blogs')->with('blogs',$blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
public function createblog()
    {
        //
        return "Blog Created";
    }

    public function create()
    {
        //



        return view('User/createblog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */







    public function store(Request $request)
    {
     

//   $request->validate(

// ['image'=> 'mimes:jpg,png']
//   );

$Blog = new Blog;


$Blog->title = $request->input('Title');

$Blog->body = $request->input('Description');

$Blog->email= $request->input('email');
$Blog->name= $request->input('name');


if($request->hasfile('image'))

{

    $file = $request->file('image');
    $extension =  $file->getClientOriginalExtension();
    $filename = time() . "-" . $extension;
    $file->move('uploads/blog/', $filename);
    $Blog->image= $filename;


     if ($extension == 'png' )
     {


    $Blog->save();
    return redirect('/User/blogs')->with('success','Blog Created Successfully!');

    }


    elseif($extension == 'jpg')
    {

        return redirect('/User/blogs')->with('success','Blog Created Successfully!');

    }
    else
    {
    return redirect()->back()->with('success','Please upload jpg or png format!');

    }




    

}


   $Blog->save();
    return redirect('User/blogs')->with('success','Blog Created Successfully!');






}
    















    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
            $blogs = Blog::find($id);
        


        return view('User/show')->with('blogs',$blogs);
           
             
    }   
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

         $blog = Blog::find($id);
  
         return view('User/edit' ,compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
if($request->hasfile('image')){

    $Blog = Blog::find($id);





$Blog->title = request('Title');

$Blog->body = request('Description');

$Blog->email= request('email');

$Blog->image= request('image');
$Blog->name= request('name');



    $file = $Blog->image;
    $extension =  $file->getClientOriginalExtension();
    $filename = time() . "-" . $extension;
    $file->move('uploads/blog/', $filename);
    $Blog->image= $filename;

     if ($extension != 'png' || 'jpg' ){


    return redirect()->back()->with('success','Please upload jpg or png format!');

  

    }


}

else{

$Blog = Blog::find($id);



$Blog->title = request('Title');

$Blog->body = request('Description');

$Blog->email= request('email');


}


$Blog->save();

        

  return redirect('/User/blogs')->with('success','Blog Updated Successfully!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      
        DB::delete('delete from blogs where id=?',[$id]);
        return redirect('/User/blogs')->with('success','Blog Deleted Successfully!');
    }

  



  public function allblogs()
    {
        //

        $blogs = Blog::all();
      

       

      

        
     return view('User/userallblogs')->with('blogs',$blogs);
    }


   



 public function allblogshow($id)
    {
        //

          $blogs = Blog::find($id);
          $comments = comment::all();



        return view('User/allblogshow')
        ->with('blogs',$blogs)
         ->with('comments',$comments)
 
         ->with('success','Comment Added Successfully!');


    }
        }
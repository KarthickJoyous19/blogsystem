<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Schedule;
use App\Models\Blog;
use App\Models\User;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
            $schedules = schedule::find($id);

        return view('/User/showblogs')->with('schedules',$schedules);
           
             
    }   

 public function createschedule()
    {
        //
        return view('/User/createschedule');
    }

    public function schedule(Request $request)
    {
     



  
$schedules = new schedule;
$schedules->title = $request->input('Title');

$schedules->body = $request->input('Description');

$schedules->email= $request->input('email');
$schedules->scheduled= $request->input('time');
$schedules->name= $request->input('name');



        $date = date_create($schedules->scheduled);



         if( date_format($date, 'Y-m-d H:i') <= date('Y-m-d H:i') )

{

 // return redirect('/createschedule')->with('success','Please choose correct time!');

  return redirect()->back()->with('success','Please choose correct time!');


}


if($request->hasfile('image')){

    $file = $request->file('image');
    $extension =  $file->getClientOriginalExtension();
    $filename = time() . "-" . $extension;
    $file->move('uploads/blog/', $filename);
    $schedules->image= $filename;

   if ($extension == 'png' )
     {


    $schedules->save();
    return redirect('/User/scheduleblogs')->with('success','Schedule Blog Created Successfully!');

    }


    elseif($extension == 'jpg')
    {
        $schedules->save();
        return redirect('/User/scheduleblogs')->with('success','Schedule Blog Created Successfully!');

    }
    else
    {
    return redirect()->back()->with('success','Please upload jpg or png format!');

    }
}


$schedules->save();

        return redirect('/User/scheduleblogs')->with('success','Schedule Blog Created Successfully!');
    }


public function index()
    {
        //

        $schedules = schedule::all();
      

        
     return view('/User/scheduleblogs')->with('schedules',$schedules);
    }


public function edit($id)
    {
        //

         $Schedule = Schedule::find($id);
         return view('/User/editblogs' ,compact('Schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
if($request->hasfile('image')){

    $Schedule = Schedule::find($id);



$Schedule->title = request('Title');

$Schedule->body = request('Description');

$Schedule->email= request('email');

$Schedule->image= request('image');

$Schedule->scheduled = request('time');
$Schedule->name = request('name');





    $file = $Schedule->image;
    $extension =  $file->getClientOriginalExtension();
    $filename = time() . "-" . $extension;
    $file->move('uploads/blog/', $filename);
    $Schedule->image= $filename;




    if ($extension == 'png' )
     {


   $Schedule->save();
    return redirect('/User/scheduleblogs')->with('success','Schedule Blog Created Successfully!');

    }


    elseif($extension == 'jpg')
    {
       $Schedule->save();
        return redirect('/User/scheduleblogs')->with('success','Schedule Blog Created Successfully!');

    }
    else
    {
    return redirect()->back()->with('success','Please upload jpg or png format!');

    }


}

else{

$Schedule = Schedule::find($id);



$Schedule->title = request('Title');

$Schedule->body = request('Description');

$Schedule->email= request('email');

$Schedule->scheduled = request('time');
$Schedule->name = request('name');



}


$Schedule->save();

        

  return redirect('/User/scheduleblogs')->with('success','Schedule Blog Updated Successfully!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::delete('delete from schedules where id=?',[$id]);
        return redirect('/User/scheduleblogs')->with('success','Schedule Blog Deleted Successfully!');
    }
}









 
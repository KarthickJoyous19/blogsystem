<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;


class DashboardController extends Controller
{
   public function index()
   {
       if(Auth::user()->hasRole('blogwriter'))
       {
            return view('User.dashboard');
       }
       elseif(Auth::user()->hasRole('admin'))
       {
             return view('AdminLTE.dashboard');
   }

   }













   public function myprofile()
   {

          $roles= Role::all();
    return view('myprofile', [

     'roles'=> $roles,
    ]


);
   }

   public function postcreate()
   {
    return view('postcreate');
   }
}



<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class UserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   


     public function handle($request, Closure $next)



{
    if ($request->route('edit'))
     {
        $user = User::find($request->route('edit'));
        if ($user && $user->id != auth()->user()->id)


         {
            abort(403);
        }
    }

    return $next($request);
}   









    
}
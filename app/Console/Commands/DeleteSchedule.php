<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Schedule;
use App\Models\Blog;
use App\Models\User;

class DeleteSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Delete:Schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()

    {

           $Schedules = Schedule::all();

        foreach($Schedules as $Schedule)
      {

  
     

        $id=$Schedule->id;
        $title=$Schedule->title;
        $body=$Schedule->body;
        $email=$Schedule->email;
        $image=$Schedule->image;
        $created=$Schedule->created_at;
        $updated=$Schedule->updated_at;
        $Scheduled=$Schedule->scheduled;
        $name=$Schedule->name;

     

        $date = date_create($Scheduled);



         if( date_format($date, 'Y-m-d H:i') <= date('Y-m-d H:i') )

        {


             DB::table('schedules')

             ->where( 'id' , $id )

              ->delete();

         }

         else
         {
            echo "No Schedules";
         }
     }



    }
}
